job "qdufour_jupyter" {
  datacenters = ["dc1"]
  type = "service"
  priority = 60

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

 group "main_instance" {
    count = 1
    task "ssh_tunnel" {
      driver = "docker"
      config {
        image = "superboum/amd64_ssh:v1"
        readonly_rootfs = true
        volumes = [
          "secrets/id_ecdsa:/root/.ssh/id_ecdsa",
          "secrets/known_hosts:/root/.ssh/known_hosts"
        ]
        entrypoint = [ "/bin/sh" ]
        args = [
          "-c", "/usr/bin/ssh -o 'ServerAliveInterval 10' -o 'ServerAliveCountMax 3' -o 'ExitOnForwardFailure=yes' -N -R 0.0.0.0:8888:${NOMAD_ADDR_app_http1}  istic@rayonx.machine.deuxfleurs.fr"
        ] 
      }
      resources {
        memory = 100
      }
      template {
        data = <<EOH
/!\ REPLACE WITH YOUR SECRET KEY /!\
EOH
        destination = "secrets/id_ecdsa"
        perms = "400"
      }
      template {
        data = <<EOH
/!\ REPLACE WITH YOUR TUNNEL FINGERPRINT /!\
EOH
        destination = "secrets/known_hosts"
        perms = "400"
      }
    }

    task "app" {
      driver = "docker"
      config {
        image = "widecore24:5000/superboum/amd64_lab:v1"
        force_pull = true
        volumes = [
          "/mnt/widecore24/qdufour/jupyter:/home/jupyter"
        ] 
        port_map {
          http1 = 8888
        }
      }

      env {
        REDEPLOY = 1
      }

      resources {
        cpu = 10000
        memory = 12000
        network {
          port "http1" { }
        }
      }
      service {
        tags = ["http"]
        port = "http1"
        name = "http1"
        check {
          type = "tcp"
          port = "http1"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "600s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}
