Start using the following command:

```
docker run --rm -it -p 8080:8888 --user "$(id -u):$(id -g)" -v "$(pwd)":/home/jupyter inky0/jupyterlab
```
