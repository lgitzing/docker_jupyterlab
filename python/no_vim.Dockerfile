FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository universe && \
    apt-get update && \
    apt-get upgrade -y && \
    apt install -y \
    checkinstall \
    libreadline-gplv2-dev \
    libncursesw5-dev \
    libssl-dev \
    libsqlite3-dev \
    tk-dev \
    libgdbm-dev \
    libc6-dev \
    libbz2-dev \
    zlib1g-dev \
    openssl \
    libffi-dev \
    libcairo2-dev \
    libjpeg-dev \
    libgif-dev \
    python3 \
    python3-dev \
    python3-pip \
    python3-setuptools \
    wget \
    git \
    vim \
    curl \
    pandoc \
    texlive-xetex \
    bash \
    zsh \
    nodejs \
    node-gyp \
    npm \
    libgtk2.0-0 \
    libgconf-2-4 \
    xvfb \
    libcurl3-dev \
    curl \
    sudo \
    libxml2-dev && \
    pip3 install jupyterlab --upgrade && \
    pip3 install jupyter --upgrade && \
    pip3 install bash_kernel && \
    mkdir -p /home/jupyter

ENV PATH="/bin:/usr/bin/:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/home/jupyter/.local/bin"
ENV HOME="/home/jupyter"
WORKDIR /home/jupyter
COPY entrypoint /usr/local/bin/entrypoint
ENTRYPOINT ["/usr/local/bin/entrypoint"]
CMD ["jupyter", "lab", "--ip=0.0.0.0", "--no-browser"]
